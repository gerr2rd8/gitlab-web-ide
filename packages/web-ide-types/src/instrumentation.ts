export interface RemoteConnectionStartTrackingEvent {
  name: 'remote-connection-start';
}

export interface RemoteConnectionSuccessTrackingEvent {
  name: 'remote-connection-success';
}

export interface RemoteConnectionFailureEvent {
  name: 'remote-connection-failure';
}

export type TrackingEvent =
  | RemoteConnectionStartTrackingEvent
  | RemoteConnectionSuccessTrackingEvent
  | RemoteConnectionFailureEvent;
