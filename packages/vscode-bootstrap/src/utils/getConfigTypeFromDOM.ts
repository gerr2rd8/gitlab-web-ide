import { ConfigType } from '@gitlab/web-ide-types';
import { createError } from './error';

const CONFIG_EL_ID = 'gl-config-type';

export const getConfigTypeFromDOM = (): ConfigType => {
  const el = document.getElementById(CONFIG_EL_ID);

  if (!el) {
    throw createError(`Could not find element for config in document (${CONFIG_EL_ID}).`);
  }

  const type = el.dataset.settings;

  if (!type) {
    throw createError(`Could not find "data-settings" in config element (${CONFIG_EL_ID}).`);
  }

  // what: We `===` here (instead of `!==`) so that typescript will blow
  //       up if our values do not align with what's declared for ConfigType
  if (type === 'client-only' || type === 'remote') {
    return type;
  }

  throw createError(`Unsupported config type ${type}.`);
};
