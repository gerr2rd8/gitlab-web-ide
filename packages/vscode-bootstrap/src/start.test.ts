import { ErrorType } from '@gitlab/web-ide-types';
import webIdeExtensionMeta from '@gitlab/vscode-extension-web-ide/vscode.package.json';
import { postMessage } from '@gitlab/vscode-mediator-commands';
import { createClientOnlyConfig, createRemoteHostConfig } from '@gitlab/utils-test';
import vscodeVersion from '@gitlab/vscode-build/vscode_version.json';
import { useMockAMDEnvironment } from '../test-utils/amd';
import { startClientOnly, startRemote } from './start';
import { WorkbenchModule } from './vscode';

const CLIENT_ONLY_TEST_CONFIG = createClientOnlyConfig();
const REMOTE_TEST_CONFIG = createRemoteHostConfig();

jest.mock('@gitlab/vscode-mediator-commands');

describe('vscode-bootstrap start', () => {
  const amd = useMockAMDEnvironment();

  const workbenchDisposeSpy = jest.fn();
  const workbenchModule = {
    create: jest.fn(),
    events: {
      onWillShutdown: jest.fn(),
    },
    URI: {
      // TODO: Chad "This is weird"
      parse: (x: string) => `URI.parse-${x}`,
      from: (x: Record<string, string>) => `URI.from-${x.scheme}-${x.path}-${x.authority}}`,
    },
    logger: {
      log: jest.fn(),
    },
  };
  const bufferModule = {};

  const triggerOnWillShutdown = () => {
    const [handler] = workbenchModule.events.onWillShutdown.mock.calls[0];

    handler();
  };

  beforeAll(() => {
    amd.shim();
  });

  beforeEach(() => {
    // We have to do spy setup like mockReturnValue in a `beforeEach`. Otherwise
    // Jest will clear it out after the first run.
    workbenchModule.create.mockReturnValue({ dispose: workbenchDisposeSpy });

    amd.define<WorkbenchModule>('vs/workbench/workbench.web.main', () => workbenchModule);
    amd.define('vs/base/common/buffer', () => bufferModule);
  });

  afterEach(() => {
    amd.cleanup();
  });

  describe('startClientOnly', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let subject: any;

    beforeEach(async () => {
      await startClientOnly(CLIENT_ONLY_TEST_CONFIG);

      expect(workbenchModule.create).toHaveBeenCalledTimes(1);

      [[, subject]] = workbenchModule.create.mock.calls;
    });

    it('creates workbench on body', () => {
      expect(workbenchModule.create).toHaveBeenCalledWith(document.body, subject);
    });

    it('creates workbench with enabledExtensions', () => {
      const { publisher, name } = webIdeExtensionMeta;

      expect(subject).toMatchObject({
        enabledExtensions: [`${publisher}.${name}`],
      });
    });

    it('creates a workbench with a custom windowIndicator', () => {
      expect(subject).toMatchObject({
        windowIndicator: {
          label: '$(gitlab-tanuki) GitLab',
          command: 'gitlab-web-ide.open-remote-window',
        },
      });
    });

    it('creates workbench with trusted domains', () => {
      expect(subject).toMatchObject({
        additionalTrustedDomains: [
          'gitlab.com',
          'about.gitlab.com',
          'docs.gitlab.com',
          'gitlab.com',
          'foo.bar',
        ],
      });
    });

    it('has default layout', () => {
      expect(subject).toMatchObject({
        defaultLayout: {
          force: true,
          editors: [],
        },
      });
    });

    it('sets commit to same as vscode_version.json', () => {
      expect(subject.productConfiguration.commit).toBe(vscodeVersion.commit);
    });
  });

  describe('startRemote', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let subject: any;

    beforeEach(() => {
      startRemote(REMOTE_TEST_CONFIG);

      expect(workbenchModule.create).toHaveBeenCalledTimes(1);

      [[, subject]] = workbenchModule.create.mock.calls;
    });

    it('creates workbench on body', () => {
      expect(workbenchModule.create).toHaveBeenCalledWith(document.body, subject);
    });

    it('creates workbench without extra extensions', () => {
      expect(subject).not.toHaveProperty('enabledExtensions');
    });

    it('sets remote development properties', () => {
      expect(subject).toMatchObject({
        remoteAuthority: REMOTE_TEST_CONFIG.remoteAuthority,
        connectionToken: REMOTE_TEST_CONFIG.connectionToken,
      });
    });

    it('adds custom workspace provider', async () => {
      expect(subject.workspaceProvider.workspace.folderUri).toBe(
        `URI.from-vscode-remote-${REMOTE_TEST_CONFIG.hostPath}-${REMOTE_TEST_CONFIG.remoteAuthority}}`,
      );
      await expect(subject.workspaceProvider.open()).resolves.toBe(false);
    });

    it('handles onWillShutdown event', () => {
      expect(workbenchModule.events.onWillShutdown).toHaveBeenCalledWith(expect.any(Function));
    });

    // why: base assertion for other tests that postMessage is not called by default
    it('sends a web-ide-tracking message containing a remote-connection-start tracking event', () => {
      expect((postMessage as jest.Mock).mock.calls).toHaveLength(1);
      expect(postMessage).toHaveBeenCalledWith({
        key: 'web-ide-tracking',
        params: {
          event: {
            name: 'remote-connection-start',
          },
        },
      });
    });

    // why: base assertion for other tests that dispose is not called by default
    it('workbench dispose should not be called', () => {
      expect(workbenchDisposeSpy).not.toHaveBeenCalled();
    });
  });

  describe('when onWillShutdown event is triggered', () => {
    beforeEach(() => {
      startRemote(REMOTE_TEST_CONFIG);
    });

    it('when onWillShutdown is triggered, posts close message', () => {
      triggerOnWillShutdown();

      expect(postMessage).toHaveBeenCalledWith({
        key: 'close',
      });
    });
  });

  describe('when startRemote raises an exception', () => {
    const error = new Error();

    beforeEach(() => {
      workbenchModule.create.mockImplementationOnce(() => {
        throw error;
      });
      startRemote(REMOTE_TEST_CONFIG);
    });

    it('posts START_WORKBENCH_FAILED error message', () => {
      expect(postMessage).toHaveBeenCalledWith({
        key: 'error',
        params: { errorType: ErrorType.START_WORKBENCH_FAILED },
      });
    });

    it('logs the workbench creation error', () => {
      expect(workbenchModule.logger.log).toHaveBeenCalledWith(expect.any(Number), String(error));
    });
  });
});
