import type { IFileContentProvider } from '@gitlab/web-ide-fs';
import { stripPathRoot } from '../stripPathRoot';

/**
 * Decorator for IFileContentProvider that strips repo root from calls to getContent
 */
export class FileContentProviderWithRepoRoot implements IFileContentProvider {
  readonly #base: IFileContentProvider;

  readonly #repoRoot: string;

  constructor(base: IFileContentProvider, repoRoot: string) {
    this.#base = base;
    this.#repoRoot = repoRoot;
  }

  getContent(path: string): Promise<Uint8Array> {
    return this.#base.getContent(stripPathRoot(path, this.#repoRoot));
  }
}
