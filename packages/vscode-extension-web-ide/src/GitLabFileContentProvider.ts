import { RateLimiter } from 'limiter';
import { IFileContentProvider } from '@gitlab/web-ide-fs';
import { fetchFileRaw } from './mediator';

export class GitLabFileContentProvider implements IFileContentProvider {
  readonly #ref: string;

  readonly #limiter: RateLimiter;

  constructor(ref: string) {
    this.#ref = ref;

    // Limit to 30 requests every 6 seconds - https://gitlab.com/gitlab-org/gitlab-web-ide/-/issues/52#note_1203173080
    this.#limiter = new RateLimiter({ tokensPerInterval: 30, interval: 6000 });
  }

  async getContent(path: string): Promise<Uint8Array> {
    // why: We need to RateLimit this while we investigate handling large folder
    //      renamed https://gitlab.com/gitlab-org/gitlab-web-ide/-/issues/52
    await this.#limiter.removeTokens(1);

    const vsbuffer = await fetchFileRaw(this.#ref, path);

    return vsbuffer.buffer;
  }
}
