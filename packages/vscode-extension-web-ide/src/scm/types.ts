import * as vscode from 'vscode';
import { IFileStatus } from '@gitlab/web-ide-fs';

export interface IStatusViewModel {
  readonly uri: vscode.Uri;
  readonly command: vscode.Command;
  readonly decorations: {
    readonly tooltip: string;
    readonly letter: string;
    readonly color: vscode.ThemeColor;
    readonly strikethrough: boolean;
    readonly propagate: boolean;
  };
}

export interface IReadonlySourceControlViewModel {
  getCommitMessage(): string;
  getStatus(): IFileStatus[];
}

export interface ISourceControlViewModel extends IReadonlySourceControlViewModel {
  update(statuses: IFileStatus[]): void;
}

export interface CommitCommandOptions {
  readonly shouldPromptBranchName: boolean;
}

export type CommitCommand = (options?: CommitCommandOptions) => Promise<void>;
