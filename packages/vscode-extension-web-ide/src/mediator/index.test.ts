import * as vscode from 'vscode';
import { COMMAND_GET_CONFIG, COMMAND_MEDIATOR_TOKEN } from '@gitlab/web-ide-interop';
import {
  COMMAND_COMMIT,
  COMMAND_CREATE_PROJECT_BRANCH,
  COMMAND_FETCH_FILE_RAW,
  COMMAND_FETCH_MERGE_REQUEST_DIFF_STATS,
  COMMAND_FETCH_PROJECT_BRANCHES,
  COMMAND_OPEN_URI,
  COMMAND_PREVENT_UNLOAD,
  COMMAND_READY,
  COMMAND_SET_HREF,
  COMMAND_START,
  COMMAND_START_REMOTE,
} from '@gitlab/vscode-mediator-commands';
import * as mediator from './index';
import { setupFakeMediatorToken } from '../../test-utils/setupFakeMediatorToken';

const TEST_MEDIATOR_TOKEN = 'fake-mediator-token';

describe('vscode-extension-web-ide/mediator/index', () => {
  beforeEach(() => {
    setupFakeMediatorToken(TEST_MEDIATOR_TOKEN);
  });

  afterEach(() => {
    // Clear memoized things to keep test determinism
    mediator.getConfig.cache.clear?.();
  });

  it('memoizes mediator token', async () => {
    expect(vscode.commands.executeCommand).not.toHaveBeenCalled();

    await mediator.start();
    await mediator.start();
    await mediator.start();

    expect(jest.mocked(vscode.commands.executeCommand).mock.calls).toEqual([
      [COMMAND_MEDIATOR_TOKEN],
      [COMMAND_START, TEST_MEDIATOR_TOKEN, {}],
      [COMMAND_START, TEST_MEDIATOR_TOKEN, {}],
      [COMMAND_START, TEST_MEDIATOR_TOKEN, {}],
    ]);
  });

  it.each`
    name                            | expectedCommand                           | args
    ${'start'}                      | ${COMMAND_START}                          | ${[{ ref: 'test' }]}
    ${'fetchFileRaw'}               | ${COMMAND_FETCH_FILE_RAW}                 | ${['ref', 'path']}
    ${'fetchMergeRequestDiffStats'} | ${COMMAND_FETCH_MERGE_REQUEST_DIFF_STATS} | ${[{}]}
    ${'fetchProjectBranches'}       | ${COMMAND_FETCH_PROJECT_BRANCHES}         | ${[{}]}
    ${'createProjectBranch'}        | ${COMMAND_CREATE_PROJECT_BRANCH}          | ${[{}]}
    ${'ready'}                      | ${COMMAND_READY}                          | ${[]}
    ${'startRemote'}                | ${COMMAND_START_REMOTE}                   | ${[{}]}
    ${'commit'}                     | ${COMMAND_COMMIT}                         | ${[{}]}
    ${'preventUnload'}              | ${COMMAND_PREVENT_UNLOAD}                 | ${[{}]}
    ${'setHref'}                    | ${COMMAND_SET_HREF}                       | ${['test']}
    ${'openUri'}                    | ${COMMAND_OPEN_URI}                       | ${['test']}
    ${'getConfig'}                  | ${COMMAND_GET_CONFIG}                     | ${[]}
  `(
    '$name calls $expectedCommand',
    async ({
      name,
      expectedCommand,
      args,
    }: {
      name: keyof typeof mediator;
      expectedCommand: string;
      args: never[];
    }) => {
      expect(vscode.commands.executeCommand).not.toHaveBeenCalled();

      const mediatorFn: (...x: never[]) => unknown = mediator[name];
      await mediatorFn(...args);

      expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
        expectedCommand,
        TEST_MEDIATOR_TOKEN,
        ...args,
      );
    },
  );

  it('getConfig is memoized', async () => {
    const testConfig = {};

    expect(vscode.commands.executeCommand).not.toHaveBeenCalled();

    jest.mocked(vscode.commands.executeCommand).mockResolvedValue(testConfig);

    const result1 = await mediator.getConfig();
    const result2 = await mediator.getConfig();

    expect(vscode.commands.executeCommand).toHaveBeenCalledTimes(1);
    expect(result1).toBe(result2);
    expect(result1).toBe(testConfig);
  });
});
