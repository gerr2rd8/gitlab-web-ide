# `web-ide-interop`

This package contains types and constants shared with external projects, such as the [`gitlab-vscode-extension`](https://gitlab.com/gitlab-org/gitlab-vscode-extension/).
