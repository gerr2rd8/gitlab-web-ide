// eslint-disable-next-line import/no-extraneous-dependencies
import { AnyConfig, ConfigType } from '@gitlab/web-ide-types';
import { escapeCssQuotedValue } from './escapeCssQuotedValue';

import { escapeHtml } from './escapeHtml';

export const getIframeHtml = (configType: ConfigType, config: AnyConfig) => {
  const { baseUrl, nonce } = config;
  const json = JSON.stringify(config);

  const fontHeader = config.editorFont
    ? `
  <link
    rel="preload"
    as="font"
    type="font/${escapeHtml(config.editorFont.format)}"
    crossorigin
    href="${escapeHtml(config.editorFont.srcUrl)}"
  />
  <style>
    @font-face {
      font-family: '${escapeCssQuotedValue(config.editorFont.fontFamily)}';
      /* optional means extremely-small block period and no swap period */
      font-display: optional;
      src: url('${escapeCssQuotedValue(config.editorFont.srcUrl)}') format('${escapeCssQuotedValue(
        config.editorFont.format,
      )}');
      font-style: normal;
    }
  </style>
  `
    : '';

  const safeNonceAttr = nonce ? `nonce="${escapeHtml(nonce)}"` : '';

  return `<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8" />
      <!-- Disable pinch zooming -->
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no"
      />
      <meta id="gl-config-type" data-settings="${escapeHtml(configType)}" />
      <meta id="gl-config-json" data-settings="${escapeHtml(json)}" />
      <link
        data-name="vs/workbench/workbench.web.main"
        rel="stylesheet"
        href="${escapeHtml(baseUrl)}/vscode/out/vs/workbench/workbench.web.main.css"
      />
      ${fontHeader}
    </head>
    <body>
      <script src="${escapeHtml(baseUrl)}/main.js" ${safeNonceAttr}></script>
    </body>
  </html>`;
};
