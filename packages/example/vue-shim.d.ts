declare module '*.vue' {
  import { Component, ComputedOptions, MethodOptions } from 'vue';

  export default Component<unknown, unknown, unknown, ComputedOptions, MethodOptions>();
}
